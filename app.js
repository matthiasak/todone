var app;
$(function() {
	/// start by creating the object
	var app = window.app || {};
	window.app = app;
	var ENTER_KEY = 13;

	// Todone Model
	// ----------
	// Our basic **Todone** model has `title`, `order`, and `completed` attributes.
	app.Todone = Backbone.Model.extend({
		// Default attributes for the Todone
		// and ensure that each Todone created has `title` and `completed` keys.
		defaults: {
			title: '',
			time: (new Date()).toLocaleDateString(),
			points: 0
		}
	});

	// Todone Collection
	// ---------------
	// The collection of Todones is backed by *localStorage* instead of a remote
	// server.
	var TodoneList = Backbone.Collection.extend({
		// Reference to this collection's model.
		model: app.Todone,

		// Save all of the Todone items under the `"Todones"` namespace.
		localStorage: new Store('todone'),
	});

	// Create our global collection of **Todones**.
	app.Todones = new TodoneList(); // []

	// Todone Item View
	// --------------
	// The DOM element for a Todone item...
	app.TodoneView = Backbone.View.extend({
		//... is a list item tag.
		tagName:  'li',

		// Cache the template function for a single item.
		template: _.template( $('#list-item-template').html() ),

		// The DOM events specific to an item.
		events: {
			'dblclick':	'clear'
		},

		// The TodoneView listens for changes to its model, re-rendering. Since there's
		// a one-to-one correspondence between a **Todone** and a **TodoneView** in this
		// app, we set a direct reference on the model for convenience.
		initialize: function() {
			this.model.on( 'destroy', this.remove, this );
		},

		// Re-render the titles of the Todone item.
		render: function() {
			this.$el.addClass('clearfix').html( this.template( this.model.toJSON() ) );
			return this;
		},

		// Remove the item, destroy the model from *localStorage* and delete its view.
		clear: function() {
			this.model.destroy();
		}
	});

	// The Application
	// ---------------
	// Our overall **AppView** is the top-level piece of UI.
	app.AppView = Backbone.View.extend({
		// Instead of generating a new element, bind to the existing skeleton of
		// the App already present in the HTML.
		el: '#todoneapp',

		// Delegated events for creating new items, and clearing completed ones.
		events: {
			'keyup #taskname': 'createOnEnter',
			'click #taskbutton': 'create'
			// 'click #toggle-all': 'toggleAllComplete'
		},

		// At initialization we bind to the relevant events on the `Todones`
		// collection, when items are added or changed. Kick things off by
		// loading any preexisting Todones that might be saved in *localStorage*.
		initialize: function() {
			this.input = this.$('#taskname'); /// $('#todoneapp #taskname')
			
			app.Todones.on( 'add', this.addOne, this );
			app.Todones.on( 'reset', this.addAll, this ); /// IGNORE ME :)
			// app.Todones.on( 'filter', this.filterAll, this );
			// app.Todones.on( 'all', this.render, this );
			app.Todones.fetch();
		},

		// Re-rendering the App just means refreshing the statistics -- the rest
		// of the app doesn't change.
		render: function() {
		},

		// Add a single Todone item to the list by creating a view for it, and
		// appending its element to the `<ul>`.
		addOne: function( task ) {
			var view = new app.TodoneView({ model: task });
			$('#completed').append( view.render().el );
		},

		// Add all items in the **Todones** collection at once.
		addAll: function() {
			this.$('#completed').html('');
			app.Todones.each(this.addOne, this);
		},

		// If you hit return in the main input field, create new **Todone** model,
		// persisting it to *localStorage*.
		createOnEnter: function( e ) {
			if ( e.which !== ENTER_KEY) {
				return;
			}

			this.create();
		},

		create: function(){
			if(!this.input.val().trim() ) {
				this.input.val('');
				return;
			}

			app.Todones.create({ title: this.input.val() });
			this.input.val('');
		}

		// Clear all completed Todone items, destroying their models.
		// clearCompleted: function() {
		// 	_.each( app.Todones.completed(), function( Todone ) {
		// 		Todone.destroy();
		// 	});

		// 	return false;
		// }
	});

	// Todone Router
	// ----------
	var Workspace = Backbone.Router.extend({
		routes:{
			'*filter': 'setFilter'
		},

		setFilter: function( param ) {
			// Set the current filter to be used
			app.TodoneFilter = param.trim() || '';

			// Trigger a collection filter event, causing hiding/unhiding
			// of Todone view items
			app.Todones.trigger('filter');
		}
	});

	app.TodoneRouter = new Workspace();
	Backbone.history.start();



	// Kick things off by creating the **App**.
	new app.AppView();

});